$(document).on("ready", function() {
  $(".regular").slick({
    mobileFirst: true,
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 2,
    responsive: [
      {
      breakpoint: 1080,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1
        }
      }
    ]
  });
});